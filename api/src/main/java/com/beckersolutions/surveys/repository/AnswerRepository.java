package com.beckersolutions.surveys.repository;

import com.beckersolutions.surveys.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AnswerRepository extends JpaRepository<Answer, Long> {

    @Override
    Optional<Answer> findById(Long id);
}
