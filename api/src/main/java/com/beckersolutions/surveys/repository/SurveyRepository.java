package com.beckersolutions.surveys.repository;

import com.beckersolutions.surveys.model.Survey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SurveyRepository extends JpaRepository<Survey, Long> {

    List<Survey> findByCreatedByUser(Long id);
    List<Survey> findAll();

}
