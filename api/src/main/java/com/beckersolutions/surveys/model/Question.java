package com.beckersolutions.surveys.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tb_questions")
@SequenceGenerator(name="sq_questions", sequenceName="sq_questions", allocationSize=1)
@ToString
@Getter
@Setter
@NoArgsConstructor
public class Question implements Serializable {

    @Id
    @Column(name = "id_question", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_questions")
    private Long id;

    @Column(name = "txt_answer")
    private String textQuestion;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "jtb_que_ans", joinColumns = {@JoinColumn(name = "id_answer",referencedColumnName = "id_question")})
    private List<Answer> answers;

    public Question(String textQuestion, List<Answer> answers) {
        this.textQuestion = textQuestion;
        this.answers = answers;
    }

    public Question(String textQuestion) {
        this.textQuestion = textQuestion;
        this.answers = new ArrayList<>();
    }

    public void addAnswer(Answer answer){
        this.answers.add(answer);
    }
}
