package com.beckersolutions.surveys.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tb_user_answer")
@SequenceGenerator(name="sq_user_answer", sequenceName="sq_user_answer", allocationSize=1)
@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserAnswer implements Serializable {

    @Id
    @Column(name = "id_user_answer", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_answer")
    private Long id;

    @ManyToOne
    @JoinColumn
    private Survey survey;

    @ManyToOne
    @JoinColumn
    private Answer user_answer;

    @Column
    private int phone;

    @Column
    private int document;

}
