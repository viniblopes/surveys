package com.beckersolutions.surveys.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tb_answers")
@SequenceGenerator(name="sq_answer", sequenceName="sq_answer", allocationSize=1)
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Answer implements Serializable {

    @Id
    @Column(name = "id_answer", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_answer")
    private Long id;

    @Column(name = "txt_answer")
    private String textAnswer;

    public Answer(String textAnswer) {
        this.textAnswer = textAnswer;
    }
}

