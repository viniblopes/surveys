package com.beckersolutions.surveys.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tb_surveys")
@SequenceGenerator(name="sq_survey", sequenceName="sq_survey", allocationSize=1)
@Getter
@Setter
@ToString
public class Survey implements Serializable {

    @Id
    @Column(name = "id_survey", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_survey")
    private Long id;

    @Column(name = "dt_creation", nullable = false)
    private Date createDate;

    @Column(name = "dt_modify")
    private Date modifyDate;

    @Column(name = "status", nullable = false)
    private SurveyStatus status;

    @Column(name = "txt_title", nullable = false)
    private String title;

    @ManyToOne
    @JoinColumn(name = "fk_created_user", referencedColumnName = "id_user")
    private User createdByUser;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "jtb_sur_que", joinColumns = {@JoinColumn(name = "id_question",referencedColumnName = "id_survey")})
    private List<Question> questions;

    public Survey(Date createDate, SurveyStatus status, User createdByUser) {
        this.createDate = createDate;
        this.status = status;
        this.createdByUser = createdByUser;
    }

    public Survey() {
        this.createDate = new Date();
        this.status = SurveyStatus.DRAFT;
    }

    public void addQuestion(Question question) {
        this.questions.add(question);
    }

    public void removeQuestion(Question question) {
        this.questions.remove(question);
    }

    public void removeQuestion(Long id) {
        this.questions
                .removeIf(question -> question.getId().equals(id));
    }
}
