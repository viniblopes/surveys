package com.beckersolutions.surveys.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tb_users")
@SequenceGenerator(name="sq_user", sequenceName="sq_user", allocationSize=1)
@Getter
@Setter
@ToString
@NoArgsConstructor
class User implements Serializable {

    @Id
    @Column(name = "id_user", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_user")
    private Long id;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private String email;

}
