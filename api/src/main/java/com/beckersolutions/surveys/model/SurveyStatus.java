package com.beckersolutions.surveys.model;

public enum SurveyStatus {
    DRAFT,
    DOING_RESEARCH,
    DONE
}
