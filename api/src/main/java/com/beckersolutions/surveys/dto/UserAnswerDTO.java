package com.beckersolutions.surveys.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserAnswerDTO {

    private int phone;
    private int document;
    private Long id_survey;
    private Long id_answer;
}
