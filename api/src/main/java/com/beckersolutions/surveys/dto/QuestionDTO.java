package com.beckersolutions.surveys.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@ToString
public class QuestionDTO {

    private String text;
    private List<AnswerDTO> answers;

}
