package com.beckersolutions.surveys.service;

import com.beckersolutions.surveys.dto.QuestionDTO;
import com.beckersolutions.surveys.dto.SurveyDTO;
import com.beckersolutions.surveys.exception.ResourceNotFoundException;
import com.beckersolutions.surveys.model.Answer;
import com.beckersolutions.surveys.model.Question;
import com.beckersolutions.surveys.model.Survey;
import com.beckersolutions.surveys.model.SurveyStatus;
import com.beckersolutions.surveys.repository.SurveyRepository;
import net.bytebuddy.implementation.bind.MethodDelegationBinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SurveyService {

    @Autowired
    private SurveyRepository repository;

    public List<Survey> findAllSurveys() {
        return repository.findAll();
    }

    public Survey findSurvey(Long surveyId) {
        return this.repository
                .findById(surveyId)
                .orElseThrow(
                        () -> new ResourceNotFoundException("Survey " + surveyId + "not found")
                );
    }

    public Survey createSurvey(SurveyDTO surveyDTO) {
        Survey survey = convertSurveyDTO(surveyDTO);
        return this.repository.save(survey);
    }

    public ResponseEntity deleteSurvey(Long surveyId) {
        Survey surveyFound = findSurvey(surveyId);
        this.repository.delete(surveyFound);
        return ResponseEntity.ok().build();
    }

    public Survey addQuestionOnSurvey(Long surveyId, QuestionDTO questionDTO) {
        Survey surveyFound = findSurvey(surveyId);
        Question question = convertQuestionDTO(questionDTO);
        surveyFound.addQuestion(question);
        return this.repository.save(surveyFound);
    }

    public List<Question> getAllQuestionsFromSurvey(Long surveyId) {
        Survey surveyFound = findSurvey(surveyId);
        return surveyFound.getQuestions();
    }

    public Survey deleteQuestionFromSurvey(Long surveyId, Long questionId) {
        Survey surveyFound = findSurvey(surveyId);
        surveyFound.removeQuestion(questionId);
        return this.repository.save(surveyFound);
    }

    public Survey activateSurvey(Long surveyId) {
        Survey surveyFound = findSurvey(surveyId);
        surveyFound.setStatus(SurveyStatus.DOING_RESEARCH);
        return this.repository.save(surveyFound);
    }

    public Survey finishSurvey(Long surveyId) {
        Survey surveyFound = findSurvey(surveyId);
        surveyFound.setStatus(SurveyStatus.DONE);
        return this.repository.save(surveyFound);
    }

    private Survey convertSurveyDTO(SurveyDTO surveyDTO) {
        Survey survey = new Survey();
        survey.setTitle(surveyDTO.getTitle());
        return survey;
    }

    private Question convertQuestionDTO(QuestionDTO questionDTO) {
        Question question = new Question(questionDTO.getText());
        questionDTO.getAnswers().forEach(answerDTO -> {
            Answer answer = new Answer(answerDTO.getText());
            question.addAnswer(answer);
        });
        return question;
    }
}
