package com.beckersolutions.surveys.service;

import com.beckersolutions.surveys.dto.UserAnswerDTO;
import com.beckersolutions.surveys.exception.ResourceNotFoundException;
import com.beckersolutions.surveys.model.Answer;
import com.beckersolutions.surveys.model.Survey;
import com.beckersolutions.surveys.model.UserAnswer;
import com.beckersolutions.surveys.repository.AnswerRepository;
import com.beckersolutions.surveys.repository.SurveyRepository;
import com.beckersolutions.surveys.repository.UserAnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserAnswerService {

    @Autowired
    private UserAnswerRepository userAnswerRepository;

    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private AnswerRepository answerRepository;

    public UserAnswer addUserAnswer(UserAnswerDTO userAnswerDTO) {
        UserAnswer userAnswer = new UserAnswer();
        Optional<Survey> surveyFound = surveyRepository.findById(userAnswerDTO.getId_survey());
        surveyFound.ifPresentOrElse(survey -> {
                    Optional<Answer> answerFound = answerRepository.findById(userAnswerDTO.getId_answer());
                    answerFound.ifPresentOrElse(answer -> {
                        userAnswer.setUser_answer(answer);
                        userAnswer.setSurvey(survey);
                        userAnswerRepository.save(userAnswer);
                    },
                            () -> new ResourceNotFoundException("Answer " + userAnswerDTO.getId_answer() + " not found"));
                },
                () -> new ResourceNotFoundException("Survey " + userAnswerDTO.getId_survey() + " not found"));
        return userAnswer;
    }
}
