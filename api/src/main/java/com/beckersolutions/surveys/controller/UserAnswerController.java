package com.beckersolutions.surveys.controller;

import com.beckersolutions.surveys.dto.UserAnswerDTO;
import com.beckersolutions.surveys.model.UserAnswer;
import com.beckersolutions.surveys.service.UserAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/answers")
public class UserAnswerController {

    @Autowired
    private UserAnswerService service;

    @PostMapping("/")
    public UserAnswer addUserAnswer(@RequestBody UserAnswerDTO userAnswerDTO){
        return service.addUserAnswer(userAnswerDTO);
    }
}
