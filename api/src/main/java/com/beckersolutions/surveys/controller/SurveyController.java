package com.beckersolutions.surveys.controller;

import com.beckersolutions.surveys.dto.QuestionDTO;
import com.beckersolutions.surveys.dto.SurveyDTO;
import com.beckersolutions.surveys.model.Question;
import com.beckersolutions.surveys.model.Survey;
import com.beckersolutions.surveys.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/surveys")
public class SurveyController {

    @Autowired
    private SurveyService service;

    @GetMapping("/")
    public List<Survey> getAllSurveys(){
        return this.service.findAllSurveys();
    }

    @GetMapping("/{surveyId}")
    public Survey getSurvey(@PathVariable Long surveyId){
        return this.service.findSurvey(surveyId);
    }

    @PostMapping("/")
    public Survey createSurvey(@RequestBody SurveyDTO surveyDTO) {
        return this.service.createSurvey(surveyDTO);
    }

    @DeleteMapping("/{surveyId}")
    public ResponseEntity deleteSurvey(@PathVariable("surveyId") Long surveyId){
        return this.service.deleteSurvey(surveyId);
    }

    @GetMapping("/{surveyId}/questions")
    public List<Question> getAllQuestions(@PathVariable("surveyId") Long surveyId) {
        return this.service.getAllQuestionsFromSurvey(surveyId);
    }
    @PostMapping("/{surveyId}/questions")
    public Survey addQuestion(@PathVariable("surveyId") Long surveyId, @Valid @RequestBody QuestionDTO questionDTO) {
        return this.service.addQuestionOnSurvey(surveyId, questionDTO);
    }

    @DeleteMapping("/{surveyId}/questions/{questionId}")
    public Survey deleteQuestion(@PathVariable("surveyId") Long surveyId, @PathVariable("questionId") Long questionId) {
        return this.service.deleteQuestionFromSurvey(surveyId, questionId);
    }

    @PutMapping("/{surveyId}/activate")
    public Survey activateSurvey(@PathVariable Long surveyId){
        return this.service.activateSurvey(surveyId);
    }

    @PutMapping("/{surveyId}/finish")
    public Survey finishSurvey(@PathVariable Long surveyId){
        return this.service.finishSurvey(surveyId);
    }
}
